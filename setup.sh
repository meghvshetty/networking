#!/bin/bash

#git install
apt-get upgrade -y
apt-get update -y
apt-get install git -y
apt-get install firewalld -y
apt-get install network-manager -y
apt-get install ethtool -y
apt-get update -y

# Pulling Gitlab respo
git clone https://gitlab.com/meghvshetty/networking.git

# install device missing drivers 
dpkg -i ./networking/firmware-atheros_20210818-1_all.deb

# setup static interface 
mv ./networking/interface/interfaces /etc/network/interfaces

# creating service
mv ./networking/creating_access_point/logic_AP.service /etc/systemd/system/
mv ./networking/creating_access_point/logic_nmcli.py /etc/systemd/system/
systemctl daemon-reload 
systemctl enable logic_AP.service

# setup NAT
echo 1 >> /proc/sys/net/ipv4/ip_forward
sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf
firewall-cmd --permanent --direct --add-rule ipv4 nat POSTROUTING 0 -o wlo1 -j MASQUERADE
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i enp3s0 -o wlo1 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i wlo1 -o enp3s0 -m state --state RELATED,ESTABLISHED -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i enp4s0 -o wlo1 -j ACCEPT
firewall-cmd --permanent --direct --c-rule ipv4 filter FORWARD 0 -i wlo1 -o enp4s0 -m state --state RELATED,ESTABLISHED -j ACCEPT
firewall-cmd --add-masquerade --permanent

# firewall PORT FORWARDING
firewall-cmd --add-forward-port=port=8080:proto=tcp:toport=8080:toaddr=10.10.1.4 --zone=public
firewall-cmd --add-forward-port=port=7000:proto=tcp:toport=22:toaddr=10.10.1.4 --zone=public

# firewall services
firewall-cmd --zone=public --add-service=syncthing --permanent
firewall-cmd --zone=internal --add-service=http --permanent
firewall-cmd --zone=internal --add-service=https --permanent
firewall-cmd --zone=internal --add-service=ssh --permanent

# firewall zone 
firewall-cmd --zone=internal --add-interface=enp3s0
firewall-cmd --zone=internal --add-interface=enp4s0

# Installing Snort dependenices
apt install build-essential libpcap-dev libpcre3-dev libnet1-dev zlib1g-dev luajit hwloc libdnet-dev libdumbnet-dev bison flex liblzma-dev openssl libssl-dev pkg-config libhwloc-dev cmake cpputest libsqlite3-dev uuid-dev libcmocka-dev libnetfilter-queue-dev libmnl-dev autotools-dev libluajit-5.1-dev libunwind-dev

mkdir snort-source-files
cd snort-source-files

git clone https://github.com/snort3/libdaq.git
cd libdaq
./bootstrap
./configure
make
make install

# Install Snort
cd ../
git clone git://github.com/snortadmin/snort3.git

cd snort3/
./configure_cmake.sh --prefix=/usr/local

cd build
make 
make install

ldconfig     # update the lib
ln -s /usr/local/bin/snort /usr/sbin/snort # symbolic

# Creating snort interface service
mv ./networking/interface/snort3-nic.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now snort3-nic.service

# Install Snort 3 Community Rulesets 
mkdir /usr/local/etc/rules
wget https://www.snort.org/downloads/community/snort3-community-rules.tar.gz
tar xzf snort3-community-rules.tar.gz -C /usr/local/etc/rules/

# Creating Snort as a service
mv ./networking/interface/snort3.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now snort3.service

# clean up 
rm -rfd ./networking

#System reboot
reboot
