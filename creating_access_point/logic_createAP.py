from os import system
import urllib.request
import subprocess as sp
from time import time, sleep

#automate access_point to turn On and Off when there is no internet 

def connection(host='http://google.com'):                   #check the connection to the internet
    try:
        urllib.request.urlopen(host)
        return True
    except:
        return False



def ap_status():                                              #check if any device is connected to access point
    connection_status = sp.getoutput("iw dev wlan0 station dump")
    if (len(connection_status)) > 0:
        print("connected AP")
        return True
    print("No device connected to AP")
    return False


def accessPoint():                                             
    device_connected = True
    while(device_connected):
        sleep(30)
        device_connected = ap_status() #check if device connected
    system("sudo systemctl stop ap.service")
    print("AP Down")

fail_network = False

while True:
    sleep(10)
    connected = connection()
    if connected == True:
        print ("Connected")
        fail_network = False
    elif fail_network:
        print ("AP Up")
        system("sudo systemctl start ap.service")
        accessPoint()
    elif connected == False:
        fail_network = True
        print("Failed to connect once")
