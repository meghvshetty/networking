from os import system
import urllib.request
import subprocess as sp
from time import time, sleep

def connection(host='http://192.168.1.1'):          #check the connection to the admin page
    try:
        urllib.request.urlopen(host)
        return True
    except:
        return False

fail_network = True  #default boolen value

while True:
    sleep(5)
    connected = connection()
    if connected == True:
        print ("Connected")
        fail_network = False
    
    elif fail_network:                          #CONNECT TO AP 1
        print ("conneting to FoxROBTEST")
        system("nmcli device wifi connect FoxROBTEST")
        connected = connection()
        if connected == False:                   #CONNECT TO AP 2
            print("connecting to FOxROBOTEST_1")
            system("nmcli device wifi connect FoxROBTEST_1")
